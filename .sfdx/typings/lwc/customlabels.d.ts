declare module "@salesforce/label/c.MasterOrgVersion" {
    var MasterOrgVersion: string;
    export default MasterOrgVersion;
}
